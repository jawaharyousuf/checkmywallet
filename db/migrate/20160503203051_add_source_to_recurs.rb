class AddSourceToRecurs < ActiveRecord::Migration
  def change
    add_column :recurs, :source, :string, default: "Bank"
  end
end
