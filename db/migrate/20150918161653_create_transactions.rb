class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
	  t.belongs_to :report, index: true
	  t.belongs_to :user, index: true
      t.date :transaction_date
      t.float :billing_amount
      t.string :merchant
      t.string :merchant_zip
      t.integer :mcc
	  t.string :category

      t.timestamps
    end
  end
end
