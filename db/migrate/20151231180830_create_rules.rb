class CreateRules < ActiveRecord::Migration
  def change
    create_table :rules do |t|
	  t.belongs_to :user, index: true
      t.string :merchant
      t.string :category
      t.timestamps
    end
  end
end
