class AddActiveToRecurs < ActiveRecord::Migration
  def change
    add_column :recurs, :active, :boolean, default: true
  end
end
