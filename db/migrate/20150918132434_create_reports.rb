class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string :name
	  t.belongs_to :dashboard, index: true
      t.timestamps
    end
  end
end
