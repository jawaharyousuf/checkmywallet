class CreateMembers < ActiveRecord::Migration
  def change
    create_table :members do |t|
      t.belongs_to :family, index: true
	  t.string :member_name
      t.timestamps
    end
  end
end
