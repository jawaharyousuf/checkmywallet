class AddRuleIdToTransaction < ActiveRecord::Migration
  def change
	add_reference :transactions, :rules, index: true
  end
end
