class CreateRecurs < ActiveRecord::Migration
  def change
    create_table :recurs do |t|
      t.string :name
      t.integer :days
      t.date :start_date
      t.float :amount
	  t.belongs_to :user, index: true
	  
      t.timestamps
    end
  end
end
