class RenameReportToStatement < ActiveRecord::Migration
  def change
  	rename_table :reports, :statements
	rename_column :transactions, :report_id, :statement_id
  end
end
