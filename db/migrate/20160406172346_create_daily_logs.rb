class CreateDailyLogs < ActiveRecord::Migration
  def change
    create_table :daily_logs do |t|
      t.date :date
	  t.belongs_to :family, index: true
	  t.belongs_to :member, index: true
	  t.belongs_to :chore, index: true
	  t.string :comment	
	  t.timestamps
    end
  end
end
