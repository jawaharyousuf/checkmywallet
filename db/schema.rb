# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160711195834) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "chores", force: true do |t|
    t.string   "name"
    t.string   "desc"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "daily_logs", force: true do |t|
    t.date     "date"
    t.integer  "family_id"
    t.integer  "member_id"
    t.integer  "chore_id"
    t.string   "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "daily_logs", ["chore_id"], name: "index_daily_logs_on_chore_id", using: :btree
  add_index "daily_logs", ["family_id"], name: "index_daily_logs_on_family_id", using: :btree
  add_index "daily_logs", ["member_id"], name: "index_daily_logs_on_member_id", using: :btree

  create_table "dashboards", force: true do |t|
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dashboards", ["user_id"], name: "index_dashboards_on_user_id", using: :btree

  create_table "families", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "members", force: true do |t|
    t.integer  "family_id"
    t.string   "member_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "members", ["family_id"], name: "index_members_on_family_id", using: :btree

  create_table "recurs", force: true do |t|
    t.string   "name"
    t.integer  "days"
    t.date     "start_date"
    t.float    "amount"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "source",     default: "Bank"
    t.boolean  "active",     default: true
  end

  add_index "recurs", ["user_id"], name: "index_recurs_on_user_id", using: :btree

  create_table "rules", force: true do |t|
    t.integer  "user_id"
    t.string   "merchant"
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rules", ["user_id"], name: "index_rules_on_user_id", using: :btree

  create_table "statements", force: true do |t|
    t.string   "name"
    t.integer  "dashboard_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "statements", ["dashboard_id"], name: "index_statements_on_dashboard_id", using: :btree

  create_table "transactions", force: true do |t|
    t.integer  "statement_id"
    t.integer  "user_id"
    t.date     "transaction_date"
    t.float    "billing_amount"
    t.string   "merchant"
    t.string   "merchant_zip"
    t.integer  "mcc"
    t.string   "category"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "rules_id"
  end

  add_index "transactions", ["rules_id"], name: "index_transactions_on_rules_id", using: :btree
  add_index "transactions", ["statement_id"], name: "index_transactions_on_statement_id", using: :btree
  add_index "transactions", ["user_id"], name: "index_transactions_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.integer  "family_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "approved",               default: false, null: false
  end

  add_index "users", ["approved"], name: "index_users_on_approved", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["family_id"], name: "index_users_on_family_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
