class DailyLog < ActiveRecord::Base
	belongs_to :family
	belongs_to :member
	belongs_to :chore
	def member
		Member.where(id: self.member_id).first
	end
	def chore
		Chore.where(id: self.chore_id).first
	end
end
