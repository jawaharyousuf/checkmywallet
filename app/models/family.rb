class Family < ActiveRecord::Base
	has_many :users, dependent: :destroy
	has_many :members, dependent: :destroy
	has_many :daily_logs, dependent: :destroy
	
	def seed
		members = [ "Yousuf",
					"Shafreen",
					"Jawahar",
					"Other",
					"Yousuf|Shafreen",
					"Yousuf|Jawahar",
					"Yousuf|Shafreen|Jawahar",
					"Jawahar|Shafreen"
					]
		members.each do |member|
			self.members.create!(member_name: member)
		end
	end
end
