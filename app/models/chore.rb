class Chore < ActiveRecord::Base
	has_many :daily_log, dependent: :destroy
	
	def seed
		chores = ["Laundry Wash",
				  "Laundry Dry",
				  "Laundry Fold",
				  "Cook",
				  "Sweep",
				  "Mop",
				  "Vaccum",
				  "Clean Stove (EasyOff)",
				  "Clean Kitchen Counter (Lysol)",
				  "Clean Kitchen Sink (Detergent)",
				  "Clean Microwave (Lysol)",
				  "Wash Dishes",
				  "Dishwasher Load",
				  "Dishwasher Empty",
				  "Clean Fridge",
				  "Grocery Organizing",
				  "Kitchen Garbage Disposal",
				  "Clean Dining Table (Lysol)",
				  "Organize Living Room",
				  "Organize Bedroom",
				  "Bedroom Garbage Disposal",
				  "Clean Toilet Bowl (Scrubbing Bubbles)",
				  "Clean Bathtub/shower (Scrubbing Bubbles)",
				  "Clean Bathroom counter,sink,taps (Scrubbing Bubbles)",
				  "Clean Bathroom mirror (Windex)",
				  "Bathroom Garbage Disposal",
				  "Lawn Mowing",
				  "Snow Shovelling",
				  "Clean Garage",
				  "House Garbage Disposal",
				  "Repair or Installation work",
				  "Date night",
				  "Vacation",
				  "Outing",
				  "Errand"]
				  
		chores.each do |chore|
			Chore.create!(name: chore)
		end
	end
end
