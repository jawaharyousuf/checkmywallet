class Member < ActiveRecord::Base
	has_many :daily_logs, dependent: :destroy
end
