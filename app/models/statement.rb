class Statement < ActiveRecord::Base    
	has_many :transactions, dependent: :destroy
	def strip_merchant(merchant)
	    puts merchant
		output = merchant.strip.scan(/\d+{13}|\D+{13}/).last
		output ||= merchant
		output.strip
    end
	def pcmastercard_profile(f,user_id)
		f.each_line do |line|
				csvline = CSV.parse_line(line.gsub(/,\s+\"/,',"'))
				transaction_params={:statement_id => self.id,
						transaction_date: Date.strptime(csvline[0], "%m/%d/%Y").to_s,
						billing_amount:csvline[2].gsub(/[\)$]/,'').gsub(/[\(]/,'-'),
						merchant: csvline[3],
						merchant_zip: csvline[6],
						mcc: csvline[9],
						category: Transaction::get_category(csvline[9].to_i, user_id),
						user_id: user_id
				}
				transaction = self.transactions.create(transaction_params)
		end
	end
	def cibc_profile(f,user_id)
		uncategorized = 0
		f.each_line do |line|
			csvline = CSV.parse_line(line.gsub(/,\s+\"/,',"'))
			merchant = strip_merchant(csvline[1])
			if csvline[4].nil?
				rule = Transaction::get_rule(merchant, user_id)
				if rule.nil?
					category = "0"
					uncategorized = uncategorized + 1
					rule_id = nil
				else
					category = rule.category
					rule_id = rule.id
				end
			else
				rule = Rule.create({merchant: merchant, category: csvline[4], user_id: user_id}) unless Rule.exists?(user_id: user_id, merchant: merchant)
				category = csvline[4]
				rule_id = nil
			end	
			transaction_params={:statement_id => self.id,
					transaction_date: Date.strptime(csvline[0], "%Y-%m-%d").to_s,
					billing_amount:"#{csvline[2].nil? ? csvline[3] : csvline[2]}".gsub(/[\)$]/,'').gsub(/[\(]/,'-'),
					merchant: csvline[1],
					merchant_zip: 0,
					mcc: 0,
					category: category,
					user_id: user_id,
					rules_id: rule_id
			}	
			transaction = self.transactions.create(transaction_params)
		end
		uncategorized
	end
	
	def manual_profile(f,user_id)
		f.each_line do |line|
		csvline = CSV.parse_line(line.gsub(/,\s+\"/,',"'))
		transaction_params={:statement_id => self.id,
				transaction_date: Date.strptime(csvline[0], "%m/%d/%Y").to_s,
				billing_amount:csvline[1].gsub(/[\)$]/,'').gsub(/[\(]/,'-'),
				merchant: csvline[2],
				merchant_zip: csvline[3],
				mcc: csvline[4],
				category: csvline[5],
				user_id: user_id
		}
		transaction = self.transactions.create(transaction_params)
		end
	end
	
	
	def import(file, user_id)
		f = file.open
		uncategorized = 0
		headers = CSV.parse_line(f.readline)
		puts headers, headers.length, headers.include?(" SICMCC Code")
		if headers.include?(" SICMCC Code") and headers.length == 10
			#PC Mastercard Profile detected
			pcmastercard_profile(f, user_id)
		elsif headers.include?("Debit") and headers.length == 5
			uncategorized = cibc_profile(f,user_id)
		elsif headers.include?("billing_amount") and headers.length == 6
			manual_profile(f,user_id)
		end
		f.close
		uncategorized
	end	
		
end
