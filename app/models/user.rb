class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable, :timeoutable
  has_one :dashboard, dependent: :destroy
  belongs_to :family
  has_many :transactions
  has_many :rules, dependent: :destroy
 # has_many :daily_log, dependent: :destroy
  has_many :recurs, dependent: :destroy
  
  def family
	Family.where(id: self.family_id).first
  end
  
  def active_for_authentication? 
    super && approved? 
  end 

  def inactive_message 
    if !approved? 
      :not_approved 
    else 
      super # Use whatever other message 
    end 
  end
  
end
