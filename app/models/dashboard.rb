class Dashboard < ActiveRecord::Base
	belongs_to :user
	has_many :statements, dependent: :destroy
end
