json.array!(@recurs) do |recur|
  json.extract! recur, :id, :name, :days, :start_date, :amount
  json.url recur_url(recur, format: :json)
end
