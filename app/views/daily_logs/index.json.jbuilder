json.array!(@daily_logs) do |daily_log|
  json.extract! daily_log, :id, :date
  json.url daily_log_url(daily_log, format: :json)
end
