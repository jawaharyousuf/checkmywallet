json.array!(@statements) do |statement|
  json.extract! statement, :id, :month, :year
  json.url statement_url(statement, format: :json)
end
