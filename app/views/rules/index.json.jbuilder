json.array!(@rules) do |rule|
  json.extract! rule, :id, :merchant, :category
  json.url rule_url(rule, format: :json)
end
