include ActionView::Helpers::NumberHelper
class DashboardController < ApplicationController
  def index
  end
  
  def group_by_month(options={})
	start_date = Date.strptime(options[:from], "%Y-%m-%d")
	end_date = options[:to]
	options={}
	main_categories = ["Mortgage", "Shopping", "Eat-out", "Entertainment", "Utilities", "Groceries", "Car expenses","Insurance" ]
	income_savings_categories = ["Income", "Savings"]
	options[:cur_date] = start_date
	options[:end_date] = end_date
	options[:categories] = main_categories
	@main_transactions = group_by_category(options)
	@averages = average_out(@main_transactions)
	@maximums = maximums(start_date,end_date,main_categories)
	@recurring = periodic_payments(start_date, end_date)
	options[:categories] = income_savings_categories
	@income_savings = group_by_category(options)
	options[:categories] = current_user.transactions.pluck(:category).uniq - (main_categories+income_savings_categories+["Payment"])
	@others = group_by_category(options)
  end
  
  def average_out(monthly_sums)
	averages = {}
	count = monthly_sums.length
	count.times do |month|
		monthly_sums[month][:data].to_h.each_pair do |category,amount| 
			averages[category] ||=0
			averages[category] = averages[category] + (amount/count)
		end
	end
	averages
  end

  def maximums(start_date,end_date,main_categories)
	maximums = {}
	main_categories.each do |cur_category|
		 max_trans = current_user.transactions.where(transaction_date: start_date..end_date, category: cur_category).order("billing_amount DESC").first
		 if max_trans
			merchant = strip_merchant(max_trans.merchant)
			maximums[cur_category] ||= {"Date" => max_trans.transaction_date,
									 "Merchant" => merchant,
									 "Amount" => max_trans.billing_amount,
									 "Location" => max_trans.merchant_zip}
		end
	end
	maximums
  end
  
  def group_by_category(options={})
	cur_date = options[:cur_date]
	end_date = options[:end_date]
	cur_date_limit = cur_date.at_end_of_month
	categories = options[:categories]
	transactions = []
	while(cur_date <= end_date) do
			trans = current_user.transactions.where(transaction_date: cur_date..cur_date_limit)
			trans_spent = trans.sum(:billing_amount)
			trans = trans.select(:category).group(:category).sum(:billing_amount)
			trans.delete_if { |k,v| !categories.include?(k) }
			categories.each do |category|
				trans[category] ||= 0
				trans[category] = trans[category].round
			end
			trans = trans.sort.to_h
			transactions <<= {name: Date::MONTHNAMES[cur_date.month]+", #{cur_date.year}", data: trans.to_a} 
			cur_date=cur_date.next_month.at_beginning_of_month
			cur_date_limit = (cur_date.at_end_of_month <= end_date)? cur_date.at_end_of_month : end_date
	end	
	transactions
  end
  def cash_flow(balance,start_date,end_date)
	change_on_date={}
	balance_data={}
	@table_data=[]
	trans = current_user.transactions.where(transaction_date: start_date..end_date)
	trans.each do |tr|
		date = tr.transaction_date
		change_on_date[date] ||=0
		if tr.category == "Payment" and false
		elsif tr.category == "Income"
			change_on_date[date]= change_on_date[date]+tr.billing_amount 
			@table_data << [date, tr.category,tr.merchant, tr.billing_amount] if params[:report][:export]=="1"
		else
			change_on_date[date]= change_on_date[date]-tr.billing_amount
			@table_data << [date, tr.category,tr.merchant, -1*tr.billing_amount] if params[:report][:export]=="1"
		end

	end
	
	change_on_date.keys.sort.each do |date|
		balance = balance + change_on_date[date]
		date_to_show = date.to_date.strftime("%b %-d %Y")
		balance_data[date_to_show] = balance.round(2)
	end
	balance_data
  end
  
  def show_report
	@transactions = current_user.transactions.where(transaction_date: Date.strptime(params[:report][:from], "%Y-%m-%d").to_s..Date.strptime(params[:report][:to], "%Y-%m-%d").to_s).order(:transaction_date)
	if @transactions.length !=0
		@date = @transactions.last.transaction_date
		@transactions = current_user.transactions.where(transaction_date: Date.strptime(params[:report][:from], "%Y-%m-%d").to_s..Date.strptime(params[:report][:to], "%Y-%m-%d").to_s)
		@counts = @transactions.select(:category).group(:category).count
		@counts.delete("Payment")
		@counts.delete("Income")
		@counts.delete("Savings")
		@counts = @counts.sort_by {|_key,value| value}.to_h 
		@totals = @transactions.select(:category).group(:category).sum(:billing_amount)
		@total_spent = @transactions.sum(:billing_amount)
		@totals.delete("Payment")
		@totals.delete("Income")
		@totals.delete("Savings")	
		@totals = @totals.sort_by {|_key,value| value}.to_h
		@cash_flow = cash_flow(params[:report][:balance].to_f,Date.strptime(params[:report][:from], "%Y-%m-%d").to_s,Date.strptime(params[:report][:to], "%Y-%m-%d").to_s)
		group_by_month(options={from: params[:report][:from], to: @date})
	else
		redirect_to action: index
	end
  end
  
  def periodic_payments(start_date, end_date)
	recurring= []
	transactions = current_user.transactions.where(transaction_date: start_date..end_date)
	billing_amounts = transactions.pluck(:billing_amount).uniq
	billing_amounts.each do |billing_amount|
		skip_one = true
		frequency = 0
		period = 0
		merchant = ""
		start = 0
		transactions.where(billing_amount: billing_amount).order(:transaction_date).limit(3).each do |tr|
			if skip_one
				skip_one = false
				start = tr.transaction_date
				next
			end
			cur_date = tr.transaction_date
			if (start + 1.months) == cur_date
				frequency=4
				period="Every month on #{start.day.ordinalize}"
			elsif (start + 2.weeks) == cur_date
				frequency=2
				next_date = Date.today + (14-(((Date.today - start).to_i)%14))
				period="Every two weeks on #{start.strftime('%A')}. Next on #{next_date}"
			elsif (start + 1.weeks) == cur_date
				frequency=1
				period="Every week on #{start.strftime('%A')}"
			else
				frequency=0
			end
			if frequency !=0
				start = tr.transaction_date
				merchant = tr.merchant
			end
		end
		if frequency !=0 
			recurring << {merchant: strip_merchant(merchant), billing_amount: billing_amount, period: period}
		end
	end
	recurring
  end
  
end
