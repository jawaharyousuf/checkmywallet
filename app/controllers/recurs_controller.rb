class RecursController < ApplicationController
  before_action :set_recur, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
	order_by_valid= [:name, :days, :start_date, :amount, :source, :active]
	if !params[:order_by].nil? and order_by_valid.include?(params[:order_by].to_sym)
		order_by = params[:order_by].to_sym
	else
		order_by = :start_date
	end
    @recurs = current_user.recurs.all.order(order_by)
    respond_with(@recurs)
  end

  def show
    respond_with(@recur)
  end

  def new
    @recur = Recur.new
    respond_with(@recur)
  end
  
  def edit
  end

  def create
    @recur = current_user.recurs.new(recur_params)
    @recur.save
	respond_to do |format|
		if @recur.save
			format.html { redirect_to new_recur_path, notice: 'Pattern was successfully saved.' }
			response = { message: 'Pattern was successfully saved.' }
			format.json { render json: response.to_json, status: :ok }
		else
			format.html { render :recurs_path, notice: 'Pattern save failed' }
			format.json { render json: @recur.errors, status: :unprocessable_entity}
		end
	end
  end

  def update
	respond_to do |format|
      if @recur.update(recur_params) 
        format.html { redirect_to recurs_path, notice: 'Pattern was successfully updated.' }
		response = { message: 'Pattern was successfully updated.' }
        format.json { render json: response.to_json, status: :ok }
      else
        format.html { render :recurs_path, notice: 'Pattern save failed' }
		response = { message: 'Pattern save failed.' }
        format.json { render json: response.to_json, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @recur.destroy
    respond_with(@recur)
  end

  def show_report
	balance = params[:report][:balance].to_f
	from = Date.strptime(params[:report][:from], "%Y-%m-%d")
	to = Date.strptime(params[:report][:to], "%Y-%m-%d")
	@source=params[:report][:source]
	if ((to-Date.today).to_i)<0 then
		redirect_to root_path
	else
		@balance_graph = simulate(balance,from,to,@source)
	end
  end
  
  def simulate(balance,from,to,source)
  	recurs = current_user.recurs.where(source: source, active: true)
	balance_data={}
	# balance_data[Date.today.to_time(:utc).strftime("%b %-d %Y")]=balance
	balance_data[from.to_time(:utc).strftime("%b %-d %Y")]=balance
	from=from.to_date
	change_on_date={}
	categories = recurs.pluck(:name)
	@event_graph = []
	@table_data=[]
	@total_income = 0
	@total_expenses = 0
	recurs.each do |recur|
		category_data={}
		add_to_timeline= "
				if day.saturday? or day.sunday?
					day = day.next #saturday or sunday
					day = day.next if day.sunday? #if saturday add another day
				end
				date = day.to_time(:utc).to_s
				date_to_show = date.to_date.strftime(\"%b %-d %Y\")
				category_data[date_to_show]=-1*recur.amount
				change_on_date[date] ||=0
				change_on_date[date]= change_on_date[date]+recur.amount
				@table_data << [date, recur.name, -1*recur.amount]
				@total_income = @total_income + recur.amount if recur.amount < 0 
				@total_expenses = @total_expenses + recur.amount if recur.amount > 0 
		"
		#handling monthly events. They don't happen aligned to day count (e.g., every month on 10th may be 30 or 31 or 28 or 29 days away)
		if (recur.days % 30 == 0) then 
			month_num = recur.days/30
			if recur.start_date.future?
				next_date = recur.start_date
			else
				next_date = recur.start_date + (((from - recur.start_date).to_f / recur.days).ceil * month_num).months
				if (next_date-recur.start_date).to_f > 90
					#update the start_date record to account for month drift after 3 months (happens due to 31 day months, where day count results in next_date being >3.01 months away, which will be ceiled to 4 months, causing one next-date to be missed)
					recur.start_date = next_date
					recur.save
				end
			end

			((((to-next_date).to_i)/recur.days)+1).times do |i|	
				day = (next_date+(i*month_num).months)
				if day == from or day.future?
					eval add_to_timeline
				end
			end
		elsif recur.days < 0 then #one-time patterns
			if recur.start_date.future? then
				day = recur.start_date
				eval add_to_timeline
			end
		else #all other patterns
			if recur.start_date.future? then
				next_date = recur.start_date
			else
				next_date = from + (recur.days-(((from - recur.start_date).to_i)%recur.days))
				if (next_date-recur.start_date).to_f > 90
					#update the start_date record to account for month drift after 3 months (happens due to 31 day months, where day count results in next_date being >3.01 months away, which will be ceiled to 4 months, causing one next-date to be missed)
					recur.start_date = next_date
					recur.save
				end
			end			
			(next_date..to).step(recur.days).each do |day|
				eval add_to_timeline
			end
		end
		@event_graph << {name: recur.name, data: category_data.sort.to_a}
	end
	change_on_date.keys.sort.each do |date|
		balance = balance - change_on_date[date]
		date_to_show = date.to_date.strftime("%b %-d %Y")
		balance_data[date_to_show] = balance
	end
	balance_data
  end
  
  private
    def set_recur
      @recur = current_user.recurs.find(params[:id])
    end

    def recur_params
      params.require(:recur).permit(:name, :days, :start_date, :amount, :source, :active)
    end
end
