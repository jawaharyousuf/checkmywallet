require 'securerandom'
class RegistrationsController < Devise::RegistrationsController
  def create
	super
	resource.dashboard = Dashboard.create!
	resource.family_id = Family.create!(name: SecureRandom.base64).id
	resource.save
	#resource.family.seed
	sign_in(resource_name, resource)
  end
  
  def destroy
	if User.where(family_id: resource.family_id).length == 1
		Family.where(id: resource.family_id).first.destroy
	end
	super
  end
end