class RulesController < ApplicationController
  before_action :set_rule, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @rules = current_user.rules.all
    respond_with(@rules)
  end
    
  def show
    respond_with(@rule)
  end

  def new
    @rule = current_user.rules.new
    respond_with(@rule)
  end

  def edit
  end

  def create
    @rule = current_user.rules.new(rule_params)
    @rule.save
    respond_with(@rule)
  end

  def reevaluate_transactions(rule)
	transactions = current_user.transactions.where(rules_id: rule.id)
	transactions.each do |transaction|
		if transaction.category != rule.category
			transaction.category = rule.category
			transaction.save
		end
	end
  end
  def update
    @rule.update(rule_params)
	reevaluate_transactions(@rule)
	redirect_to rules_path
  end

  def destroy
    @rule.destroy
    respond_with(@rule)
  end

  private
    def set_rule
      @rule = current_user.rules.find(params[:id])
    end

    def rule_params
      params.require(:rule).permit(:merchant, :category)
    end
end
