class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @transactions = current_user.transactions.all
    respond_with(@transactions)
  end
  
  def uncategorized
    @transactions = current_user.transactions.where(category: "0")
	@categories = (current_user.transactions.pluck(:category).uniq - ["0"]) 
	if @categories.empty?
		@categories = ["Car expenses", "Shopping", "Eat-out", "Entertainment", "Utilities", "Groceries", "Insurance", "Mortgage", "Income", "Savings", "Payment", "Misc"]
	end
	if @transactions.length == 0
		redirect_to statements_path
	else
		respond_with(@transactions)
	end
  end
  

  def show
    respond_with(@transaction)
  end

  def new
    @transaction = Transaction.new
    respond_with(@transaction)
  end

  def edit
  end

  def create
    @transaction = Transaction.new(transaction_params)
    @transaction.save
    respond_with(@transaction)
  end
  
  def evaluate_other_transactions
    
	uncategorized = current_user.transactions.where(category: "0")
	uncategorized.each do |transaction|
		category = Transaction::get_category(strip_merchant(transaction.merchant), current_user.id)
		if category != "0" 
			transaction.category = category
			transaction.save
		end
	end
  end

  def update
	if category != "" then
		merchant = strip_merchant(@transaction.merchant).strip
		rule = Rule.where(user_id: current_user.id, merchant: merchant).first
		rule ||= Rule.create({merchant: merchant, category: category, user_id: current_user.id})
		transaction_params[:rules_id]=rule.id
		@transaction.update(transaction_params)
		evaluate_other_transactions
	end
	redirect_to trans_uncategorized_path
  end
  
  def update_bulk
	transactions = params[:transaction] || {}
	transactions.each_pair do |id, category|
		if category != "" then
			transaction = current_user.transactions.where(id: id.to_i).first
			merchant = strip_merchant(transaction.merchant).strip
			rule = Rule.where(user_id: current_user.id, merchant: merchant).first
			rule ||= Rule.create({merchant: merchant, category: category, user_id: current_user.id})
			transaction.update(category: category, rules_id: rule.id)
		end
	end
	evaluate_other_transactions
	redirect_to trans_uncategorized_path
  end
  
  def destroy
    @transaction.destroy
    respond_with(@transaction)
  end

  private
    def set_transaction
      @transaction = current_user.transactions.find(params[:id])
    end

    def transaction_params
      params[:transaction].permit(:category)
    end
end
