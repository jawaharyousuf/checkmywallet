class StatementsController < ApplicationController
  before_action :set_statement, only: [:show, :edit, :update, :destroy]

  # GET /statements
  # GET /statements.json
  def index
    @statements = current_user.dashboard.statements.all
  end
    
  # GET /statements/1
  # GET /statements/1.json
  def show
  end

  # GET /statements/new
  def new
    @statement = current_user.dashboard.statements.new
  end

  # GET /statements/1/edit
  def edit
  end

  # POST /statements
  # POST /statements.json
  def create
    @statement = current_user.dashboard.statements.new(statement_params)
	@statement.save
	id=@statement.id
	begin
		uncategorized = @statement.import(params[:file], current_user.id)
	rescue Exception => e
		@statement.destroy
		raise e
	end
    respond_to do |format|
      if @statement
		if uncategorized == 0
			format.html { redirect_to statements_url, notice: 'Statement was successfully created.' }
			format.json { render :show, status: :created, location: @statement }
		else
			format.html { redirect_to "/trans_uncategorized", alert: 'Some transactions are not categorized' }
			format.json { render :show, status: :created, location: @statement }
		end
      else
        format.html { render :new }
        format.json { render json: @statement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /statements/1
  # PATCH/PUT /statements/1.json
  def update
    respond_to do |format|
      if @statement.update(statement_params)
        format.html { redirect_to statements_url, notice: 'Statement was successfully updated.' }
        format.json { render :show, status: :ok, location: @statement }
      else
        format.html { render :edit }
        format.json { render json: @statement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /statements/1
  # DELETE /statements/1.json
  def destroy
    @statement.destroy
    respond_to do |format|
      format.html { redirect_to statements_url, notice: 'Statement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_statement
      @statement = current_user.dashboard.statements.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def statement_params
      params.require(:statement).permit(:name)
    end
end
