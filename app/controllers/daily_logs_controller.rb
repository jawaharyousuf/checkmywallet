class DailyLogsController < ApplicationController
  before_action :set_daily_log, only: [:show, :edit, :update, :destroy]

  # GET /daily_logs
  # GET /daily_logs.json
  def index
    @daily_logs = current_user.family.daily_logs.all.paginate(:page => params[:page], :per_page => 30).order("date DESC")
  end

  # GET /daily_logs/1
  # GET /daily_logs/1.json
  #def show
  #end

  # GET /daily_logs/new
  def new
    @daily_log = DailyLog.new
	@users = User.all
	@chores = Chore.all
  end
  
  def new_logs
	@users = User.all
	@chores = Chore.all
  end

  # GET /daily_logs/1/edit
  def edit
  	@users = User.all
	@chores = Chore.all
  end

  def import_chores
  end
  
  #get
  def show_report
	from = Date.strptime(params[:report][:from], "%Y-%m-%d").to_s
	to = Date.strptime(params[:report][:to], "%Y-%m-%d").to_s
	@daily_logs = current_user.family.daily_logs.where(date: from..to).order(:date)
	if @daily_logs.last
		@date = @daily_logs.last.date
	end
	@general_activity=activity_graph(from,to, "chore_id NOT IN (32,33,34)")
	@cooking_activity=activity_graph(from,to, {chore_id: Chore.where(id: 4)})
	@cleaning_activity=activity_graph(from,to, "chore_id NOT IN (32,33,34,4)")
	@entertainment_activity = activity_graph(from,to, {chore_id: [32,33,34]})
	@chores = Chore.all
	@workload = workload(from,to, "chore_id NOT IN (32,33,34,4)")
  end
  
  # POST /daily_logs
  # POST /daily_logs.json
  
  def import
	f = params[:file].open
	f.each_line do |line|
		csvline = CSV.parse_line(line.gsub(/,\s+\"/,',"'))
		daily_log_params={
			date: Date.strptime(csvline[0], "%m/%d/%Y").to_s,
			member_id: current_user.family.members.where(member_name: csvline[1]).first.id,
			chore_id: Chore.where(name: csvline[2]).first.id,
			comment: csvline[3]
		}
		log = current_user.family.daily_logs.new(daily_log_params)
		log.save
	end
	f.close
	redirect_to daily_logs_path
  end

  def create_logs
	params[:chore].each_pair do |chore_id, comment|
		filtered_params= {chore_id: chore_id,
						  comment: comment}
		@daily_log = current_user.family.daily_logs.new(daily_log_params)
		@daily_log.save
		@daily_log.update(filtered_params)
	end
	redirect_to daily_logs_new_logs_path, notice: 'Daily log was successfully created.'
  end
  
  def create
    @daily_log = current_user.family.daily_logs.new(daily_log_params)
    respond_to do |format|
      if @daily_log.save
        format.html { redirect_to new_daily_log_path, notice: 'Daily log was successfully created.' }
        format.json { render :show, status: :created, location: @daily_log }
      else
        format.html { render :new }
        format.json { render json: @daily_log.errors, status: :unprocessable_entity }
      end
    end
  end

  def who_did_last
	chore_id = params[:chore_id].to_i
	from = Date.strptime(params[:from], "%Y-%m-%d").to_s
	to = Date.strptime(params[:to], "%Y-%m-%d").to_s
	daily_log = current_user.family.daily_logs.where(chore_id: chore_id, date: from..to).order(:date).last
	if daily_log.nil? then
		results = {data: "No one. Ever."}
	else
		member_name = daily_log.member.member_name
		date = daily_log.date
		results = {data: "#{member_name} on #{date}. #{daily_log.comment}"}
	end
	respond_to do |format|
		format.html
		format.json {render json: results}
	end 
  end
  
  def they_did_last
	chore_id = params[:chore_id].to_i
	member_id = params[:member_id].to_i
	from = Date.strptime(params[:from], "%Y-%m-%d").to_s
	to = Date.strptime(params[:to], "%Y-%m-%d").to_s
	daily_log = current_user.family.daily_logs.where(chore_id: chore_id,member_id: member_id, date: from..to).order(:date).last
	if daily_log.nil? then
		results = {data: "Never Ever."}
	else
		date = daily_log.date
		results = {data: "On #{date}. #{daily_log.comment}"}
	end
	respond_to do |format|
		format.html
		format.json {render json: results}
	end 
  end
  
  # PATCH/PUT /daily_logs/1
  # PATCH/PUT /daily_logs/1.json
  def update
    respond_to do |format|
      if @daily_log.update(daily_log_params)
        format.html { redirect_to daily_logs_path, notice: 'Daily log was successfully updated.' }
        format.json { render :show, status: :ok, location: @daily_log }
      else
        format.html { render :edit }
        format.json { render json: @daily_log.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /daily_logs/1
  # DELETE /daily_logs/1.json
  def destroy
    @daily_log.destroy
    respond_to do |format|
      format.html { redirect_to daily_logs_url, notice: 'Daily log was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_daily_log
      @daily_log = current_user.family.daily_logs.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def daily_log_params
      params.require(:daily_log).permit(:date, :chore_id, :member_id, :comment, :page)
    end	
	
	  
	def activity_graph(from, to,options={})
		members = current_user.family.members.pluck(:id, :member_name).sort
		activity = {}
		member_sql={}
		members.each do |member_id, member_name|
			member_sql[:member_id.to_s]=member_id
			activity[member_name] = DailyLog.where(member_sql).where(options).group_by_day(:date, range: from..to, format: "%d %b %y").count
		end
		activity
	end
	
	def workload(from, to, options={})
		members = current_user.family.members.pluck(:id, :member_name).sort
		activity = {}
		@activity_by_chore = []
		activities=DailyLog.joins(:chore,:member).includes(:chore,:member).where(date: from..to).group(:member_name).count(:name)
		members.each do |member_id, member_name|
			if member_name.index("|").nil?
				activity[member_name] ||= activities[member_name]
			else
				member_name.split("|").each do |member_split|
					activity[member_split] ||= 0
					activity[member_split] = activity[member_split]+activities[member_name].to_i
				end
			end	
		end
		activities_by_chore_data = DailyLog.joins(:chore,:member).includes(:chore,:member).where(date: from..to).group(:member_name,:name).count(:name)	
		hash={}
		activities_by_chore_data.each_pair do |key,value|
			hash[key[0]] ||= {}
			hash[key[0]][key[1]]=value
		end
		hash.each_pair do |member_name,chores|
			activity_point={:name=>member_name,:data=>chores.to_a}
			@activity_by_chore << activity_point
		end

		total_activities = activity.inject(0) {|sum,hash| sum + hash[1].to_i}
		activity.map {|member,total|
			[member,(((((total.to_f)/(total_activities))*10000).round).to_f)/100] #calculating workload as percentage rounded to two decimal places
		}
	end
	
end
