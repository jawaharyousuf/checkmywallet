class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
   
  before_action :authenticate_user!
  protect_from_forgery with: :exception
  helper_method :strip_merchant
  
  def strip_merchant(merchant)
		output = merchant.strip.scan(/\d+{13}|\D+{13}/).last
		output ||= merchant
		output.strip
  end
end
