Rails.application.routes.draw do
  get 'recurs/show_report'
  get 'daily_logs/show_report'
  get 'daily_logs/import_chores'
  get 'daily_logs/new_logs'
  post 'daily_logs/who_did_last'
  post 'daily_logs/they_did_last'
  post 'daily_logs/import'
  post 'daily_logs/create_logs'
  post 'transactions/update_bulk'
  
  resources :recurs  
  resources :daily_logs, :only => [:new, :update,:edit, :destroy, :index]

  resources :transactions, :only => [:update]

  resources :rules, :only => [:new, :update, :destroy, :index]  

  devise_for :users, :controllers => { registrations: 'registrations'}

  get 'dashboard/index'
  get 'dashboard/show_report'

  resources :statements
  get 'trans_uncategorized' => 'transactions#uncategorized'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'dashboard#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
